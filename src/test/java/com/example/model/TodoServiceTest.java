package com.example.model;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TodoServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    public TodoDao todoDao;

    @InjectMocks
    public TodoService todoService;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testGetAll() throws Exception {
        List<Todo> listTodo = this.getTodoList();
        when(this.todoDao.getAll()).thenReturn(listTodo);
        List<Todo> result = this.todoService.getTodos();
        verify(this.todoDao).getAll();
        assertEquals(result.get(0).getTodoString(), "task1");
    }

    @Test
    public void testGetAllWithNullResult() throws Exception {
        List<Todo> zeroListTodo = new ArrayList<Todo>();
        when(this.todoDao.getAll()).thenReturn(zeroListTodo);
        List<Todo> result = this.todoService.getTodos();
        verify(this.todoDao).getAll();
        assertTrue(result.isEmpty());
    }

    @Test
    public void testGhangeStatus() {
        Todo openTodo = this.getTodoList().get(0);

    }

    public List<Todo> getTodoList() {
        List<Todo> list = new ArrayList<>();

        Todo todo1 = new Todo();
        todo1.setTodoString("task1");
        todo1.setStatus(TodoStatus.OPEN);
        todo1.setCreatedAt(new Date());

        Todo todo2 = new Todo();
        todo2.setTodoString("task1");
        todo2.setStatus(TodoStatus.OPEN);
        todo2.setCreatedAt(new Date());

        list.add(todo1);
        list.add(todo2);

        return list;
    }
}
