package com.example.model;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TodoDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Todo> getAll() {
        Query query = entityManager.createQuery("SELECT t FROM Todo t", Todo.class);
        return query.getResultList();
    }

    // todo(egor): transaction scope is responsibility of service layer
    public void insert(Todo todo) {
        entityManager.persist(todo);
        entityManager.flush();
    }

    public Todo update(Todo todo) {
        return entityManager.merge(todo);
    }

    public Todo findById(int id) {
        return entityManager.find(Todo.class, id);
       // Query query = entityManager.createQuery("Select t FROM Todo t WHERE t.id=:id", Todo.class).setParameter("id", id);
       // return query.getSingleResult();
    }

    public void remove(Todo todo) {
        entityManager.remove(todo);
    }
}
