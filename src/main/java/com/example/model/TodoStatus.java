package com.example.model;


public enum TodoStatus {
    OPEN ("OPEN"),
    IN_PROGRESS ("IN_PROGRESS"),
    DONE ("DONE");

    private String value;

    private TodoStatus(String val) {
        this.value = val;
    }

    public static TodoStatus fromValue(String value) {
        for (TodoStatus todoStatus: values()) {
            if (value.equalsIgnoreCase(todoStatus.value)) {
                return todoStatus;
            }
        }

        throw new IllegalArgumentException("Unknown enum type:" + value);
    }


}
