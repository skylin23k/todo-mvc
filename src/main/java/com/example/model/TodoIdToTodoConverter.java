package com.example.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class TodoIdToTodoConverter implements Converter<String, Todo> {

    private TodoService todoService;

    @Autowired
    public TodoIdToTodoConverter(TodoService todoService) {
        this.todoService = todoService;
    }

    @Override
    public Todo convert(String source) {
        try {
            int id = Integer.valueOf(source);
            return todoService.findById(id);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
