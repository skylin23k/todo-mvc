package com.example.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class TodoService {

    private TodoDao todoDao;

    @Autowired
    public TodoService(TodoDao todoDao) {
        this.todoDao = todoDao;
    }

    @Transactional
    public void create(Todo todo) {
        todo.setCreatedAt(new Date());
        todo.setStatus(TodoStatus.OPEN);
        this.todoDao.insert(todo);
    }

    @Transactional
    public void edit(Todo todo) {
        this.todoDao.update(todo);
    }

    public List<Todo> getTodos() {
        return this.todoDao.getAll();
    }

    @Transactional
    public void remove(Todo todo) {
        this.todoDao.remove(todo);
    }

    @Transactional
    public void changeStatus(Todo todo, TodoStatus todoStatus) {
        todo.setStatus(todoStatus);
        todoDao.update(todo);
    }

    public Todo findById(int id) {
        return todoDao.findById(id);
    }
}
