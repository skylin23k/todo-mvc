package com.example.presenter;

import com.example.model.Todo;

import java.util.ArrayList;
import java.util.List;

public class ListedTodoPresenter {
    private int id;
    private String todoString;
    private String status;
    private String date;

    public ListedTodoPresenter(Todo todo) {
        this.id = todo.getId();
        this.todoString = todo.getTodoString();
        this.status = todo.getStatus().toString();
        this.date = todo.getCreatedAt().toString();
    }

    public static List<ListedTodoPresenter> convertToList(List<Todo> items) {
        List<ListedTodoPresenter> result = new ArrayList<>(items.size());

        for (Todo item: items) {
            result.add(new ListedTodoPresenter(item));
        }

        return result;
    }

    public int getId() {
        return id;
    }

    public String getTodoString() {
        return todoString;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }
}
