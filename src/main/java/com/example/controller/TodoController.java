package com.example.controller;

import com.example.model.TodoService;
import com.example.presenter.ListedTodoPresenter;
import com.example.model.Todo;
import com.example.model.TodoDao;
import com.example.model.TodoStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

@Controller
public class TodoController {

    private TodoService todoService;

    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return  "index";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addAction() {
        return "add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addAction(@Validated Todo todo, BindingResult result, Model model) {
       // Todo todo = new Todo();
       // todo.setTodoString(todoString);

        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            StringBuilder errorString = new StringBuilder();
            for (ObjectError error:errors) {
                errorString.append(error.getDefaultMessage());
            }
            model.addAttribute("error", errorString.toString());

            return "add";
        } else {
            this.todoService.create(todo);
        }

        return "redirect:/";
    }
}
