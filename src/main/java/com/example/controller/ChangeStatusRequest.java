package com.example.controller;

import com.example.model.Todo;
import com.example.model.TodoStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

public class ChangeStatusRequest {
    @NotNull
    private TodoStatus status;

    @NotNull
    private Todo todo;

    public TodoStatus getStatus() {
        return status;
    }

    public Todo getTodo() {
        return todo;
    }

    public void setStatus(TodoStatus status) {
        this.status = status;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }
}
