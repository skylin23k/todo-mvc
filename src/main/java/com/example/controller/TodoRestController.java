package com.example.controller;

import com.example.model.Todo;
import com.example.model.TodoService;
import com.example.presenter.ListedTodoPresenter;
import com.example.validator.ChangeStatusRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/rest")
public class TodoRestController {

    private TodoService todoService;
    private ChangeStatusRequestValidator changeStatusRequestValidator;

    @Autowired
    TodoRestController(TodoService todoService, ChangeStatusRequestValidator validator) {
        this.todoService = todoService;
        this.changeStatusRequestValidator = validator;
    }

    // todo(egor): how about more complex paramter types? (enum, entity...)
    // todo(egor): reason for user ResponseEntity?
    // todo(egor): try use javax.validation on parameters?
    @RequestMapping(value = "/status")
    @ResponseStatus(HttpStatus.OK)
    public void status(@Validated ChangeStatusRequest request) throws RuntimeException {


        todoService.changeStatus(request.getTodo(), request.getStatus());
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<ListedTodoPresenter> list() {
        List<Todo> items = todoService.getTodos();
        return ListedTodoPresenter.convertToList(items);
    }


    @InitBinder()
    public void setChangeStatusRequestValidator(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(this.changeStatusRequestValidator);
    }
}
