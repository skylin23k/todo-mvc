package com.example.validator;

import com.example.controller.ChangeStatusRequest;
import com.example.model.Todo;
import com.example.model.TodoStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ChangeStatusRequestValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return ChangeStatusRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ChangeStatusRequest request = (ChangeStatusRequest) target;
        Todo todo= request.getTodo();
        TodoStatus status = request.getStatus();
        TodoStatus currentStatus = todo.getStatus();

        if ((status == TodoStatus.IN_PROGRESS && currentStatus != TodoStatus.OPEN) ||
        (status == TodoStatus.DONE && currentStatus != TodoStatus.IN_PROGRESS) ||
        (status == TodoStatus.OPEN && currentStatus != TodoStatus.DONE)) {
            errors.rejectValue("status", "incorrect status");
        }
    }
}
