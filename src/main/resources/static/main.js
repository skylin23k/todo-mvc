(function() {
    var underscore = _.noConflict();

    var template = '<div class="row"><div class="col-md-6"><div class="panel panel-default"><div class="panel-body">' +
                   '<span><%= todoString %></span></div><div class="panel-footer"><div class="btn-group">' +
                   '<a class="btn btn-default js_status <%= status == "OPEN" ? \'active\':\'\' %>" data-status="OPEN" data-id="<%= id %>" href="#">Open</a>' +
                   '<a class="btn btn-default js_status <%= status == "IN_PROGRESS" ? \'active\':\'\' %>" href="#" data-status="IN_PROGRESS" data-id="<%= id %>">In progress</a>' +
                   '<a class="btn btn-default js_status <%= status == "DONE" ? \'active\':\'\' %>" data-status="DONE" data-id="<%= id %>" href="#">Done</a>' +
                   '</div></div> </div> </div> </div>';

    var ItemModel = Backbone.Model.extend({
        defaults: {
            id: null,
            todoString: null,
            status: null
        }
    });

    var ItemCollection = Backbone.Collection.extend({
        model: ItemModel
    });


    var ItemView = Backbone.Marionette.View.extend({
        template: underscore.template(template),

        ui: {
            statusButton: ".js_status"
        },

        events: {
            'click @ui.statusButton': 'changeStatus'
        },

        changeStatus: function(e) {
            e.preventDefault();
            var target = $(e.currentTarget);
            var status = target.data("status");
            var id = target.data("id");
            $.ajax({
                url: "rest/status",
                type: "POST",
                data: {
                    status: status,
                    todo: id
                }
            }).then(
                function () {
                    $(ui.statusButton).removeClass("active");
                    target.addClass("active");
                },
                function () {
                }
            );
        }

    });


    var ItemCollectionView = Backbone.Marionette.CollectionView.extend({
        childView: ItemView
    });

    var ui = {
        statusButton: ".js_status"
    };

    var subscribe = {
        initialize: function() {
            $.ajax({
                url: "rest/list",
                type: "GET"
            }).then(
                function(items) {
                    var collection = new ItemCollection();
                    collection.add(items);
                    var collectionView = new ItemCollectionView({
                        collection: collection
                    });
                    collectionView.render();

                    $(document.body).append(collectionView.$el);
                }
            );
            /*
             $(ui.statusButton).on("click", $.proxy(function (e) {
             e.preventDefault();
             var target = $(e.currentTarget);
             var status = target.data("status");
             var id = target.data("id");
             $.ajax({
             url: "rest/status",
             type: "POST",
             data: {
             status: status,
             id: id
             }
             }).then(
             function () {
             $(ui.statusButton).removeClass("active");
             target.addClass("active");
             },
             function () {
             }
             );
             }));*/
        }
    };

    subscribe.initialize();
})();